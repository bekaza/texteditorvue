let webpack = require('webpack')

module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        'window.Quill': 'quill'
      })
    ]
  },
  chainWebpack: config => {
    config.module
      .rule('babel')
      .test(/\.js$/)
      .exclude
      .add(/node_modules(?!\/quill-image-drop-module|quill-image-resize-module)/).end()
      .use('babel')
      .loader('babel-loader')
  }
}
